#!/usr/bin/env bash

_dockerapps_completion() {

    COMPREPLY=()
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local prev="${COMP_WORDS[COMP_CWORD-1]}"
    local argc=${#COMP_WORDS[@]}

    if [[ "${prev}" == "dockerapps" ]] ; then
        local opts="start stop test clean rebuild console status"
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        return 0

    elif [[ "${argc}" == "3" ]] ; then
        local install_dir="$(dirname $(readlink -f $(which dockerapps)))"
        local apps="$(cd ${install_dir}/../src && ls --color=never)"
        COMPREPLY=( $(compgen -W "${apps}" -- ${cur}) )
        return 0

    else
        # echo 'nothing to tab complete'
        return 1
    fi
}

complete -F _dockerapps_completion dockerapps
